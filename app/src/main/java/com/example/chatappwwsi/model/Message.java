package com.example.chatappwwsi.model;

import java.util.Date;

public class Message {
    private String user;
    private String text;
    private String userId;
    private String date;
    private String image;

    public Message() {

    }

    public Message(String user, String text, String userId, String date) {
        this.user = user;
        this.text = text;
        this.userId = userId;
        this.date = date;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImage() {
        return this.image;
    }

    public String getUser() {
        return user;
    }

    public String getText() {
        return text;
    }

    public String getUserId() {
        return userId;
    }

    public String getDate() {
        return date;
    }
}
