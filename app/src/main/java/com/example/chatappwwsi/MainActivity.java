package com.example.chatappwwsi;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.chatappwwsi.message.MessageAdapter;
import com.example.chatappwwsi.message.MessageHolder;
import com.example.chatappwwsi.model.Message;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.annotations.Nullable;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {

    private FirebaseAuth auth;
    private FirebaseUser user;
    private FirebaseFirestore database;
    private FirestoreRecyclerAdapter<Message, MessageHolder> adapter;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        auth = FirebaseAuth.getInstance();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        checkUserLoggedIn();
        registerLogoutAction();
        registerMessageSend();
        configureRecycler();
        handlePhotoUpload();
    }

    /**
     * Konfigurujemy widok Rycyclera - podpinamy adapter Firebase
     */
    private void configureRecycler() {
        database = FirebaseFirestore.getInstance();
        // Odwołanie do query, który będzie się wykonywac na bazie z wiadomościami
        Query query = database.collection("messages").orderBy("date");
        recyclerView = findViewById(R.id.msgList);
        LinearLayoutManager manager = new LinearLayoutManager(getApplicationContext());
        // Odpowiada za to, że elementy wyświetlane są od dołu, a nie od góry
        manager.setStackFromEnd(true);
        recyclerView.setLayoutManager(manager);
        adapter = new MessageAdapter(query, user.getUid(), MainActivity.this);
        recyclerView.setAdapter(adapter);
        // Automatyczne przewijanie przy dodaniu nowych elementów
        recyclerView.scrollToPosition(adapter.getItemCount());
        //Nasłuchujemy zmian w query do firebase - jeżeli result jest inny (więc dodano lub usunieto wiadomosc)
        // To przesuwamy widok do ostatniej wiadomości
        query.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                recyclerView.scrollToPosition(adapter.getItemCount());
            }
        });
    }

    /**
     * Dodanie listenera wysylania wiadomosci po kliknieciu przycisku
     */
    private void registerMessageSend() {
        findViewById(R.id.btnSend).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessage(null);
            }
        });
    }

    /**
     * Funkcja odpowiadająca za przesyłanie wiadomości do Firebase, opcjonalnie identyfikator zdjęcia w Firebase
     *
     * @param image
     */
    private void sendMessage(String image) {
        EditText input = (EditText) findViewById(R.id.txtMessage);
        String now = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());
        Message msg = new Message(user.getDisplayName(), input.getText().toString(), user.getUid(), now);
        if (image != null) {
            msg.setImage(image);
        }
        database.collection("messages").add(msg);
        input.setText("");
    }


    @Override
    public void onStart() {
        super.onStart();
        if (adapter != null)
            if (recyclerView != null && recyclerView.getAdapter() != null) {
                recyclerView.scrollToPosition(recyclerView.getAdapter().getItemCount());
            }
        adapter.startListening();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (recyclerView != null && recyclerView.getAdapter() != null) {
            recyclerView.scrollToPosition(recyclerView.getAdapter().getItemCount() - 1);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (adapter != null)
            adapter.stopListening();
    }

    /**
     * Sprawdzenie czy user jest zalogowany, jeśli nie - przekierowujemy do logowania
     */
    private void checkUserLoggedIn() {
        user = auth.getCurrentUser();
        if (user == null) {
            startActivity(new Intent(this, LoginActivity.class));
        }
    }

    /**
     * Rejestrujemy listener akcji wylogowania. Wylogowujemy użytkownika oraz wykonujemy sprawdzenie
     * zalogowania, co przekieruje nas na ekran logowania
     */
    private void registerLogoutAction() {
        findViewById(R.id.btnLogout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                auth.signOut();
                checkUserLoggedIn();
            }
        });
    }

    private void handlePhotoUpload() {
        findViewById(R.id.btnMakePhoto).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto, 1);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @androidx.annotation.Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 1:
                if (resultCode == RESULT_OK && data != null) {
                    Uri selectedImage = data.getData();
                    if (selectedImage == null) {
                        break;
                    }
                    uploadPhoto(selectedImage);
                    Toast.makeText(this, "Wybrałeś zdjęcie " + selectedImage.getPath(), Toast.LENGTH_SHORT).show();
                }
        }
    }

    /**
     * Upload zdjęcia do usługi firebase.
     * @param photo
     */
    private void uploadPhoto(Uri photo) {
        // Tworzymy obiekt do komunikacje z FirebaseStorage
        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageRef = storage.getReference();
        // Tworzymy metadane pliku, które zostaną przesłane do usługi Firebase Storage
        StorageMetadata metadata = new StorageMetadata.Builder().setContentType("image/jpeg").build();
        // Generujemy UUID zdjęcia
        String photoUri = "images/"+ UUID.randomUUID() + ".jpeg";
        // Tworzymy zadanie uploadu do Firebase. Jest to zadanie asynchroniczne, więcj bindujemy
        // się ne event onSuccess, by po przesłaniu wysłać wiadomość z odnośnikiem do FirebaseStorage
        UploadTask uploadTask = storageRef.child(photoUri).putFile(photo, metadata);
        uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                Toast.makeText(MainActivity.this, taskSnapshot.getMetadata().getPath(), Toast.LENGTH_SHORT).show();
                sendMessage(taskSnapshot.getMetadata().getPath());
            }
        });
    }
}
