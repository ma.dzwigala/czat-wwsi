package com.example.chatappwwsi;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class LoginActivity extends AppCompatActivity {

    FirebaseAuth auth;
    FirebaseUser user;
    Button loginBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_activity);
        auth = FirebaseAuth.getInstance();
        loginBtn = findViewById(R.id.btnLogin);
        registerLoginAction();
        registerGoToRegisterAction();

    }

    private void registerGoToRegisterAction() {
        findViewById(R.id.btnGoToRegister).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, Register.class));
            }
        });
    }

    /**
     * Funkcje, które wykorzystuje do blokowania i odblokowywania UI w momencie logowania,
     * by uniknąć wielu requestów
     */
    private void beginLogin() {
        loginBtn.setEnabled(false);
        findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
    }
    private void stopLogin() {
        loginBtn.setEnabled(true);
        findViewById(R.id.progressBar).setVisibility(View.GONE);
    }

    private void registerLoginAction() {

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                beginLogin();
                String email = ((EditText) findViewById(R.id.txtLoginEmail)).getText().toString().trim();
                String password = ((EditText) findViewById(R.id.txtLoginPassword)).getText().toString().trim();
                // Walidacja danych
                if (TextUtils.isEmpty(email) || TextUtils.isEmpty(password)) {
                    Toast.makeText(LoginActivity.this, "Nie wprowadzono loginu lub hasła", Toast.LENGTH_SHORT).show();
                    stopLogin();
                    return;
                }
                auth.signInWithEmailAndPassword(email, password)
                        .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                //Wykonuje się po próbie rejestracji
                                stopLogin();
                                if (task.isSuccessful()) {
                                    user = auth.getCurrentUser();
                                    Toast.makeText(LoginActivity.this, "Logowanie powiodło się!", Toast.LENGTH_SHORT).show();
                                    startActivity(new Intent(LoginActivity.this, MainActivity.class));
                                    finish();
                                } else {
                                    Toast.makeText(LoginActivity.this, "Niepoprawne dane logowania", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
            }
        });
    }

}
