package com.example.chatappwwsi;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class Register extends AppCompatActivity {

    private FirebaseUser user;
    private Button registerButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        Button goToLoginButton = findViewById(R.id.btnLogin);

        goToLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Register.this, LoginActivity.class));
            }
        });

        registerButton = findViewById(R.id.btnRegister);

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerUser();
            }
        });
    }

    private void beginRegister() {
        registerButton.setEnabled(false);
        findViewById(R.id.progressBar2).setVisibility(View.VISIBLE);
    }

    private void stopRegister() {
        registerButton.setEnabled(true);
        findViewById(R.id.progressBar2).setVisibility(View.GONE);
    }

    private void registerUser() {
        final FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        final String email = ((EditText) findViewById(R.id.email)).getText().toString();
        final String password = ((EditText) findViewById(R.id.password)).getText().toString();
        final String username = (((EditText) findViewById(R.id.username)).getText().toString());
        if (TextUtils.isEmpty(email) || TextUtils.isEmpty(password) || TextUtils.isEmpty(username)) {
            Toast.makeText(this, "Wszystkie pola muszą być wypełnione", Toast.LENGTH_SHORT).show();
            return;
        }
        beginRegister();
        firebaseAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        stopRegister();
                        if (task.isSuccessful()) {
                            user = firebaseAuth.getCurrentUser();
                            UserProfileChangeRequest profileUpdate = new UserProfileChangeRequest.Builder()
                                    .setDisplayName(username)
                                    .build();
                            user.updateProfile(profileUpdate);
                            Toast.makeText(
                                    getApplicationContext(),
                                    "Rejestracja przebiegła pomyślnie",
                                    Toast.LENGTH_SHORT
                            ).show();
                            addUserToDatabase(username);
                            Intent intent = new Intent(Register.this, MainActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            Toast.makeText(
                                    getApplicationContext(),
                                    "Błąd podczas rejestracji",
                                    Toast.LENGTH_SHORT
                            ).show();
                        }
                    }
                });
    }

    /**
     * Tworzymy wpis w bazie danych z przechowywaniem dodatkowych danych o użytkowniku
     * @param username
     */
    public void addUserToDatabase(String username) {
        FirebaseFirestore database = FirebaseFirestore.getInstance();
        String userId = user.getUid();

        HashMap<String, String> userData = new HashMap<>();
        userData.put("a1_username", username);
        userData.put("a2_email", user.getEmail());
        Map<String, Object> update = new HashMap<>();
        update.put(userId, userData);
        database.collection("users").document(userId).set(update);
    }
}
