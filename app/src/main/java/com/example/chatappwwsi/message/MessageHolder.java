package com.example.chatappwwsi.message;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.chatappwwsi.R;

public class MessageHolder extends RecyclerView.ViewHolder {
    TextView mText;
    TextView mUsername;
    TextView mDate;
    ImageView mImg;

    public MessageHolder(View itemView) {
        super(itemView);
        this.mText = itemView.findViewById(R.id.message_body);
        this.mUsername = itemView.findViewById(R.id.name);
        this.mDate = itemView.findViewById(R.id.date);
        this.mImg = itemView.findViewById(R.id.message_photo);
    }
}
