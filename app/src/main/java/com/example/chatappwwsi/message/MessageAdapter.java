package com.example.chatappwwsi.message;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.chatappwwsi.R;
import com.example.chatappwwsi.model.Message;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.Query;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

/**
 * Klasa odpowiada za stworzenie adaptera, który pozwoli tworzyć obiekty UI wiadomości
 * na podstawie pobranych danych z Firestore
 */
public class MessageAdapter extends FirestoreRecyclerAdapter<Message, MessageHolder> {

    private final int MESSAGE_IN_VIEW_TYPE  = 1;
    private final int MESSAGE_OUT_VIEW_TYPE = 2;
    private final int MESSAGE_OUT_IMAGE = 3;
    private final int MESSAGE_IN_IMAGE = 4;

    Context context;
    String userId;
    Query query;
    StorageReference storageRef;
//    StorageReference storageReference;

    /**
     * Create a new RecyclerView adapter that listens to a Firestore Query.  See {@link
     * FirestoreRecyclerOptions} for configuration options.
     */
    public MessageAdapter(Query query, String userId, Context context) {
        super(new FirestoreRecyclerOptions.Builder<Message>().setQuery(query, Message.class).build());
        this.userId = userId;
        this.query = query;
        this.context = context;
        storageRef = FirebaseStorage.getInstance().getReference();
    }

    @Override
    protected void onBindViewHolder(@NonNull MessageHolder holder, int position, @NonNull Message model) {
        final TextView mText =  holder.mText;
        final TextView mUsername = holder.mUsername;
        final TextView mDate = holder.mDate;
        if (mUsername != null) {
            mUsername.setText(model.getUser());
        }
        if (mDate != null) {
            mDate.setText("(" + model.getDate() + ")");
        }
        if (mText != null) {
            mText.setText(model.getText());
        }
        if (model.getImage() != null && holder.mImg != null) {
            Glide.with(context)
                    .setDefaultRequestOptions(new RequestOptions())
                    .load(storageRef.child(model.getImage()))
                    .into(holder.mImg);
        }
    }

    @Override
    public int getItemViewType(int position) {
        //if message userId matches current userid, set view type 1 else set view type 2
        Message msg = getItem(position);
        if(msg.getUserId().equals(userId)){
            if (msg.getImage() != null) {
                return MESSAGE_OUT_IMAGE;
            } else {
                return MESSAGE_OUT_VIEW_TYPE;
            }
        }
        if (msg.getImage() != null) {
            return MESSAGE_IN_IMAGE;
        } else {
            return MESSAGE_IN_VIEW_TYPE;
        }
    }

    /**
     * Tworzymy odpowiedni layout zależnie od tego, jaki jest to typ wiadomości, inne są dla
     * naszych wiadomości, dla innych oraz inne są wiadomości z obrazkami
     * @param parent
     * @param viewType
     * @return
     */
    @NonNull
    @Override
    public MessageHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = null;
        switch (viewType) {
            case MESSAGE_IN_VIEW_TYPE:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.their_message, parent, false);
                break;
            case MESSAGE_IN_IMAGE:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.their_message_photo, parent, false);
                break;
            case MESSAGE_OUT_VIEW_TYPE:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.my_message, parent, false);
                break;
            case MESSAGE_OUT_IMAGE:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.my_message_photo, parent, false);

        }
        return new MessageHolder(view);
    }
}
